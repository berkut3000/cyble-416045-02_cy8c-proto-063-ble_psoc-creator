# CYBLE-416045-02 Find Me Target project

This project includes the initial code Example for the CYBLE-416045-02 BLE MCU.

## Getting Started

Open PSoC creator, load the Workspace and program the Target Device in either one of the 2 cores available.

### Prerequisites

- [PSoC Creator 4.2](https://www.cypress.com/products/psoc-creator-integrated-design-environment-ide)
- [Peripheral Driver Library 3.1.0](https://www.cypress.com/documentation/software-and-drivers/peripheral-driver-library-pdl)
- [PSoC Programmer 2.8](https://www.cypress.com/documentation/software-and-drivers/psoc-programmer-archive)
- [A compatible target device](https://www.cypress.com/documentation/datasheets/cyble-416045-02-ez-ble-creator-module), like the [CY8CPROTO-063-BLE](https://www.cypress.com/documentation/development-kitsboards/psoc-6-ble-prototyping-kit-cy8cproto-063-ble)
- Android Device with activated Bluetooth and [CySmart App](https://play.google.com/store/apps/details?id=com.cypress.cysmart) installed


### Installing

Install the aforementioned software, and ensure to register the Keil Compiler.

## Programming the target

Refer to the following application notes to see how to program the device.

- [CY8CPROTO-063-BLE_PSoC(R)_6_BLE_Prototyping_Board_Guide](https://www.cypress.com/file/450941/download)

## Open the App

Once the Target device is programmed the On Board Green LED will begin to flash; for 180 seconds, looking for a Device to pair with. Open up the CySmart App.

### Pair with the device

In the Subsequent menu, all the discoverable devices will be shown. Select the one named Find Me target device to pair with it.

### Send Alerts

Once the device has successfully paired; the menu will change, displaying all the available services supported by the target. Choose the Alert Service and; from the drop-down menu in the upper-right corner, select one of the different Alerts shown.
Observe how the On Board RED LED behavior will vary depending on the chosen alert.

## Built With

* [PSoC Creator 4.2](https://www.cypress.com/products/psoc-creator-integrated-design-environment-ide)

## Versioning

GitLab is used for versioning

## Authors

* **Antonio Aguilar Mota** - *Initial work* - [AntoMota](https://gitlab.com/berkut3000)


## License

This project is licensed under ...

## Acknowledgments

* Cypress Semiconductor
